import psycopg2

from typing import List
import logging

connection = psycopg2.connect(
    dbname='company',
    user='postgres',
    password='postgres',
    host='localhost',
)


class Salary:
    @staticmethod
    def add_salaries(ids: List[int], employee_ids: List[int], amounts: List[int]):
        cursor = connection.cursor()

        for (id, employee_id, amount) in zip(ids, employee_ids, amounts):
            try:
                cursor.execute(f'INSERT INTO "Salary" VALUES ({id}, {employee_id}, {amount})')
                logging.info(f'Successfully inserted ({id}, {employee_id}, {amount}) into Salary')
            except Exception as e:
                logging.error(str(e))
                connection.rollback()
                return e

        connection.commit()
        cursor.close()


class Tests:
    @staticmethod
    def test_no_id():
        Salary.add_salaries([1, None], [1, 2], [1337, 1337])

    @staticmethod
    def test_same_id():
        Salary.add_salaries([1, 1], [1, 2], [1337, 1337])

    @staticmethod
    def test_same_employee():
        Salary.add_salaries([1, 2], [1, 1], [1337, 1337])


if __name__ == '__main__':
    Tests.test_same_employee()
